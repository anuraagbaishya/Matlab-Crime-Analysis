function[c] = getcolor(data)
m = max(data);
c = zeros(1,35);

for i = 1:35
    c(i) = data(i)/m;
end
end