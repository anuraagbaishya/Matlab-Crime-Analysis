function [] = typeOfCrime()

    types = {'Rape','Kidnappings & Abductions', 'Dowry Deaths', 'Assaults','Insult to Modesty',...
    'Cruelty by Husband/Relatives',  'Immoral Traffic', 'Indescent Representation'};
    data = xlsread('crimes_count_women_01_12.xls');
    crimes = zeros(1,8)';
    for i = 1:8
        crimes(i) = data(36*i,13);
    end
    
    bar(crimes);
    hFig = figure(1);
    set(hFig, 'position', [200, 100, 1500, 800]);
    xlabel('\bfCrimes','FontSize',15);
    ylabel('\bfNo of cases','FontSize',15);
    title('Total crimes of different types 2001 to 2012','FontSize',18);
    set(gca, 'XTick', 1:8, 'XTickLabel', types, 'XTickLabelRotation',45);
    grid on;
end

